<?php
function add_my_stylesheet() 
{
    wp_enqueue_style( 'myCSS', plugins_url( '/css/style.css', __FILE__ ) );
}

add_action('admin_print_styles', 'add_my_stylesheet');

function theme_options_panel(){
 // add_menu_page('Calendar', 'Calendar', 'manage_options', 'theme-options', 'wps_theme_func','','dashicons-calendar-alt');
  add_menu_page('Calendar', 'Calendar', 'manage_options', 'theme-options', 'wps_theme_func','dashicons-calendar-alt',10);
  //add_submenu_page( 'theme-options', 'Settings page title', 'Settings menu label', 'manage_options', 'theme-op-settings', 'wps_theme_func_settings');
  //add_submenu_page( 'theme-options', 'FAQ page title', 'FAQ menu label', 'manage_options', 'theme-op-faq', 'wps_theme_func_faq');
}
add_action('admin_menu', 'theme_options_panel');


function wps_theme_func(){
?>

	<div class="wrap">
		<h2>Calendar by Cannella Inc.</h2>
	</div>
	<div class="unit">
		<div class="datenav">
			<?php if ($_GET["navtype"] == "") { ?>
				<a href="<?=$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."&navtype=Month"?>"><div class="navbutton navbutton-sel"">Month</div></a>
			<?php } else { ?>
				<a href="<?=$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."&navtype=Month"?>"><div class="navbutton<?php if ($_GET["navtype"] == "Month") { echo " navbutton-sel";} ?>">Month</div></a>
			<?php } ?>
			<a href="<?=$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."&navtype=Year"?>"><div class="navbutton<?php if ($_GET["navtype"] == "Year") { echo " navbutton-sel";} ?>">Year</div></a>
			<a href="#"><div class="arrow-navbutton"><div class="arrow-left"></div></div></a>
			<a href="#"><div class="arrow-navbutton"><div class="arrow-right"></div></div></a>
		</div>
		<div class="dateheading">January 2021</div>	
	</div>
	<div class="weekdays">
		<div class="weekday">Sun</div>        
		<div class="weekday">Mon</div>
       	 	<div class="weekday">Tue</div>
       	 	<div class="weekday">Wed</div>
       		<div class="weekday">Thu</div>
        	<div class="weekday">Fri</div>
        	<div class="weekday">Sat</div>	
	</div>
	<div class="calendar-month">
		<div class="calendar-day-disb">30</div>
		<div class="calendar-day-disb">31</div>
        	<div class="calendar-day">1</div>
        	<div class="calendar-day">2</div>     
        	<div class="calendar-day">3</div>
		<div class="calendar-day">4</div>
        	<div class="calendar-day">5</div>
		<div class="weekdays"></div>
		<div class="calendar-day">6</div>
                <div class="calendar-day">7</div>     
                <div class="calendar-day">8</div>
                <div class="calendar-day">9</div>
                <div class="calendar-day">10</div>
                <div class="calendar-day">11</div>
                <div class="calendar-day">12</div>
                <div class="weekdays"></div>
                <div class="calendar-day">13</div>
                <div class="calendar-day">14</div>     
                <div class="calendar-day">15</div>
                <div class="calendar-day">16</div>
                <div class="calendar-day">17</div>
                <div class="calendar-day">18</div>
                <div class="calendar-day">19</div>
                <div class="weekdays"></div>
                <div class="calendar-day">20</div>
                <div class="calendar-day">21</div>     
                <div class="calendar-day">22</div>
                <div class="calendar-day">23</div>
                <div class="calendar-day">24</div>
                <div class="calendar-day">25</div>
                <div class="calendar-day">26</div>
                <div class="weekdays"></div>
                <div class="calendar-day">27</div>
                <div class="calendar-day">28</div>
                <div class="calendar-day">29</div>
                <div class="calendar-day">30</div>
                <div class="calendar-day">31</div>
                <div class="calendar-day-disb">1</div>
                <div class="calendar-day-disb">2</div>
	</div>
	<div class="line"></div>
	<div class="line"></div>
	<div class="line"></div>
	<div class="line"></div>
	<div class="line"></div>
<?php
}
